# Contributor: Oleg Titov <oleg.titov@gmail.com>
# Maintainer: Oleg Titov <oleg.titov@gmail.com>
pkgname=lighthouse
pkgver=2.5.1
pkgrel=0
pkgdesc="Ethereum 2.0 Client"
url="https://lighthouse.sigmaprime.io/"
arch="x86_64 aarch64"  # limited by upstream
license="Apache-2.0"
makedepends="cargo clang-dev cmake openssl-dev protoc"
options="!check" # disable check as it takes too long
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/sigp/lighthouse/archive/v$pkgver/lighthouse-$pkgver.tar.gz"

# secfixes:
#   2.2.0-r0:
#     - CVE-2022-0778

export OPENSSL_NO_VENDOR=true
export RUSTFLAGS="$RUSTFLAGS -L /usr/lib/"

build() {
	cargo build --release --locked
}

check() {
	cargo test --release --locked \
		--workspace \
			--exclude ef_tests \
			--exclude eth1 \
			--exclude genesis
}

package() {
	install -D -m755 "target/release/lighthouse" "$pkgdir/usr/bin/lighthouse"

	install -Dm 644 -t "$pkgdir/usr/share/doc/lighthouse" README.md
}

sha512sums="
92dc2a65438b692436f59c203a8d0bcff2757329b057598b1b7320c155aae1ac5b7cfb9e6ba8569bf1c18acf9af602ebf9e8f2b961f855d0a86ffd189f4882b1  lighthouse-2.5.1.tar.gz
"
